const { Given, When, Then } = require('@wdio/cucumber-framework');
const LoginPageActions = require('../PageActions/LoginPageActions');
const QuotationPageActions = require('../PageActions/QuotationPageActions ');
const ProfilePageActions = require('../PageActions/ProfilePageActions');



Then(/^User verifies home page and log out of application$/, async function () {

    await LoginPageActions.UserVerifiesHomePageAndLogOutOfApplication();

});

Then(/^User should not be logged into application$/, async function () {

    await LoginPageActions.VerifyUserShouldNotBeLoggedInToAppWithInvalidCredentials();

});

Then(/^User retrieves and validates the quotation$/, async function () {

    await QuotationPageActions.UserRetrievesAndValidatesQuotation();

});

Then(/^User verifies whether the profile is updated$/, async function () {

    await ProfilePageActions.UserVerifiesWhetherProfileIsEdited();

});






