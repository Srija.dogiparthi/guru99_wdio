
class QuotationPageLocators
{

   mileage="";
   Mileage=function(mileage){
       this.mileage=mileage;
   }

   get RequestQuotationOption(){
      return $('//a[@id="ui-id-2"]');
   }
   get SelectBreakdownCover(){
      return $('//select[@id="quotation_breakdowncover"]');
   }

   get WindScreenRepairOption(){
      return $('//input[@id="quotation_windscreenrepair_f"]');
   }

   get EnterIncident(){
      return $('//input[@id="quotation_incidents"]');
   }

   get EnterRegistration(){
      return $('//input[@id="quotation_vehicle_attributes_registration"]');
   }

   get EnterMileage(){
      return $('//input[@id="quotation_vehicle_attributes_mileage"]');
   }

   get EnterVehicleValue(){
      return $('//input[@id="quotation_vehicle_attributes_value"]');
   }

   get SelectParkingLocation(){
      return $('//select[@id="quotation_vehicle_attributes_parkinglocation"]');
   }

   get SelectPolicyStartYear(){
      return $('//select[@id="quotation_vehicle_attributes_policystart_1i"]');
   }


   get SelectPolicyStartMonth(){
      return $('//select[@id="quotation_vehicle_attributes_policystart_2i"]');
   }


   get SelectPolicyStartDate(){
      return $('//select[@id="quotation_vehicle_attributes_policystart_3i"]');
   }

   get SaveQuotation(){
      return $('//input[@value="Save Quotation" and @name="submit"]');
   }

   get IdentificationNo(){
      return $('//body');
   }

   get RetrieveQuotationOption(){
      return $('//a[@id="ui-id-3"]');
   }

   get EnterIdentificationNo(){
      return $('//input[@placeholder="identification number"]');
   }

   get RetrieveOption(){
      return $('//input[@id="getquote"]');
   }


   get ValidateWindScreenRepair(){
      return $('//*[text()="Windscreenrepair"]/../following-sibling::td[text()="No"]');
   }


   get ValidateRegistration(){
      return $('//*[text()="Registration"]/../following-sibling::td[text()="boss1"]');
   }

   get ValidateMileage(){
      return $('//*[text()="Annual mileage"]/../following-sibling::td[text()='+this.mileage+']');
   }

   get ValidateEstimatedValue(){
      return $('//*[text()="Estimated value"]/../following-sibling::td[text()="1000"]');
   }

   get ValidateParkingLocation(){
      return $('//*[text()="Parking Location"]/../following-sibling::td[text()="Driveway"]');
   }

   get ValidateStartOfPolicy(){
      return $('//*[text()="Start of policy"]/../following-sibling::td[text()="2020.6.1"]');
   }

   get ValidateBreakdownCover(){
      return $('//*[text()="Breakdowncover"]/../following-sibling::td[text()="Roadside"]');
   }


}

module.exports=new QuotationPageLocators();