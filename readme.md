#Guru99 Insurance#

This README would normally document whatever steps are necessary for executing the tests.

Software Requirements to Execute the Scripts

•	Workstation with windows 10 pro 
•	Download Visual Studio Code - Windows and Install in the machine
•	Install latest Chrome browser on the workstation 
•	Download | Node.js (nodejs.org) and Install


Instructions to Clone the repository:
•	Create a folder(workspace) where you want to clone the repository.
•	Open command prompt from working directory using a keywork “cmd” in the address bar of the file explorer and click on “enter”. 
•	This will open you a command prompt by pointing the current directory as working directory 
•	Clone the repository:

o	On command prompt, enter “git clone <git clone URL>”  
o	Click enter
o	Then Microsoft Authentication prompt will trigger. Please provide valid credentials to authenticate.

•	Once the Authentication is completed successfully then repository cloning will start 
•	Navigate to “guru99_wdio" folder and list of folders & files will be displayed as below 
•   Open terminal in visual studio code and enter 'npm install' top install node modules.
•   If you get any call bind error , please execute this command in terminal in VS code 'npm install call-bind' 



Instructions to Execute the scripts

•	Double click on the Run/WebTests.bat to run all scripts which is available inside the project folder.
•	Once you executed the runner bat file you will see a prompt trigger which make the scripts to run.
•	Once the execution is completed, Allure Report will be auto generated.The report can also be found under reports\ExecutionReport in project folder. 

 

Other things to be considered:

•	Please install the following extensions extensions in order to work on the VSCode and Generate Reports Manually.
    -->Batch Runner
    -->Open In Default Browser
    -->Cucumber (Gherkin) Full Support
    -->VSCode Great Icons
    --> JavaScript Debugger (Nightly) 
•	If you face any issue with the drivers not updated, 
o	Configure the latest version of the driver under package.json as follows and 
o	Open the Terminal >> New Terminal
o	run the command “npm install chromedriver”
•	If you want to open the Allure Report again manually, open the VSCode and navigate to reports\ExecutionReport and Open the index.html with ‘Open in Default Browser’.
 
