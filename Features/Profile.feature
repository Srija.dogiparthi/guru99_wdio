Feature:Profile Functionality

@TC_05
@Profile
 Scenario:User sign into application and try to edit profile and verify whether profile is updated. 
    Given User Navigates to Base url
    When User log in to application with valid credentials and try to edit profile
    Then User verifies whether the profile is updated


