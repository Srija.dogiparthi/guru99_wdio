Feature:Login Functionality

@TC_01
@Login
 Scenario:User sign into application with valid credentials. 
    Given User Navigates to Base url
    When User log in to application with valid credentials 
    Then User verifies home page and log out of application


@TC_02
@Login
 Scenario:User is unable to sign into application with invalid credentials. 
    Given User Navigates to Base url
    When User tries to login to application with invalid credentials
    Then User should not be logged into application


