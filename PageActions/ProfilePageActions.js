const ProfilePageLocators = require("../PageObjects/ProfilePageLocators");

const testData =require("../TestData_"+Environ+".json");
var chai = require('chai');  
var chaiexpect = chai.expect

var modifiedtime='';

class ProfilePageActions
{

    async UserTriesToEditProfilePage(){
        await (ProfilePageLocators.ProfileOption).waitForDisplayed();
        await (ProfilePageLocators.ProfileOption).click();
        console.log("Clicked on Profile Option");
        modifiedtime=await (ProfilePageLocators.ModifiedTime).getText();
        console.log(modifiedtime);
        await (ProfilePageLocators.EditProfileOption).click();
        console.log("Clicked on Edit Profile Option");
        await (ProfilePageLocators.SelectTitle).waitForDisplayed();
        await (ProfilePageLocators.SelectTitle).selectByVisibleText("Miss");
        console.log("Selected Title option");
        await (ProfilePageLocators.EnterSurname).setValue("Hello");
        console.log("Entered Surname");

        await (ProfilePageLocators.EnterFirstname).setValue("Tim");
        console.log("Entered Firstname");
        await (ProfilePageLocators.EnterPhone).setValue("9876543210");
        console.log("Entered Phone no");
        await (ProfilePageLocators.SelectBirthYear).selectByVisibleText("1993");
        console.log("Selected Year of Birth");
        await (ProfilePageLocators.SelectBirthMonth).selectByVisibleText("June");
        console.log("Selected Month of Birth");
        await (ProfilePageLocators.SelectBirthDate).selectByVisibleText("1");
        console.log("Selected Date of Birth");
        await (ProfilePageLocators.SelectLicenseType).click();
        console.log("Selected License Type");
        await (ProfilePageLocators.SelectLicensePeriod).selectByVisibleText("2");
        console.log("Selected License Period");
        await (ProfilePageLocators.SelectOccupation).selectByVisibleText("Academic");
        console.log("Selected Occupation");
        await (ProfilePageLocators.EnterStreet).setValue("street1");
        console.log("Entered Street text");
        await (ProfilePageLocators.EnterCountry).setValue("country1");
        console.log("Entered country text");
        await (ProfilePageLocators.EnterCity).setValue("city1");
        console.log("Entered city text");
        await (ProfilePageLocators.EnterPostcode).setValue("9090");
        console.log("Entered postcode text");
        await (ProfilePageLocators.UpdateUserButton).click();
        console.log("Clicked on Update User Button");



    }
    async UserVerifiesWhetherProfileIsEdited(){
        await (ProfilePageLocators.ProfileOption).click();
        console.log("Clicked on Profile Option");
        if(modifiedtime.includes("Last modified: November 01 2019 11:00:04")){
            console.log("Profile is not updated");
            await chaiexpect(false).to.be.true;

        }


    }

}

module.exports=new ProfilePageActions();