const LoginPageLocators = require("../PageObjects/LoginPageLocators");
const testData =require("../TestData_"+Environ+".json");

class LoginPageActions
{

async UserNavigateToBaseURL(){
    await browser.maximizeWindow();
    await browser.url(testData.BaseUrl);
    let waitforLoginPage=await LoginPageLocators.EnterEmail;
    await waitforLoginPage.waitForDisplayed({ timeout: 5000 });

}
async UserLoginToApplicationWithValidCredentials(){

    await (LoginPageLocators.EnterEmail).click();
    await (LoginPageLocators.EnterEmail).setValue(testData.Email);
    console.log("User entered email");
    await (LoginPageLocators.EnterPassword).click();
    await (LoginPageLocators.EnterPassword).setValue(testData.Password);
    console.log("User entered pasword");
    await (LoginPageLocators.LogIn).click();
    console.log("User clicked on log in button");



}
async UserVerifiesHomePageAndLogOutOfApplication(){
    await LoginPageLocators.UserName(testData.Email);
    let validusername= await (LoginPageLocators.ValidUser).getText();
    await expect(LoginPageLocators.ValidUser).toHaveText(validusername);
    console.log("User verifies the home page");
    await (LoginPageLocators.LogOut).click();
    console.log("User clicked on log out button");
    let waitforLoginPage=await LoginPageLocators.EnterEmail;
    await waitforLoginPage.waitForDisplayed({ timeout: 5000 });
    console.log("User logged out of application");


} 

async UserTriesToLoginToApplicationWithInvalidCredentials(){

    await (LoginPageLocators.EnterEmail).click();
    await (LoginPageLocators.EnterEmail).setValue(testData.Email+"&*^");
    console.log("User entered email");
    await (LoginPageLocators.EnterPassword).click();
    await (LoginPageLocators.EnterPassword).setValue(testData.Password);
    console.log("User entered pasword");
    await (LoginPageLocators.LogIn).click();
    console.log("User clicked on log in button");


}
async VerifyUserShouldNotBeLoggedInToAppWithInvalidCredentials(){

    await expect(LoginPageLocators.InvalidUserText).toBeDisplayed();
    console.log("User Verifies the error message");
}


}

module.exports=new LoginPageActions();